﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Captcha
{
    public class RandomCaptchaGenerator : IRandomCaptchaGenerator
    {
        private readonly string _SYMBOLS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        public int LEBGHTCAPCHA = 5;
        private int HEIGHTCAPCHA = 50;
        private int WIDTHCAPCHA = 160;
        private Random _random;

        public RandomCaptchaGenerator()
        {
            _random = new Random();
        }

        public Captcha CreatCaptcha()
        {
            var randomText = RandomTRext();
            Bitmap bitmap = new Bitmap(WIDTHCAPCHA, HEIGHTCAPCHA, PixelFormat.Format32bppArgb);

            using (Graphics g = Graphics.FromImage(bitmap))
            {
                g.DrawString(randomText, new Font("Arial", HEIGHTCAPCHA / 2, FontStyle.Bold),
                               Brushes.Red, new RectangleF(0, 0, WIDTHCAPCHA, HEIGHTCAPCHA));
            }

            return new Captcha(bitmap, randomText);
        }
        private string RandomTRext()
        {
           return  new string(Enumerable.Repeat(_SYMBOLS, LEBGHTCAPCHA)
              .Select(s => s[_random.Next(s.Length)]).ToArray());
        }
    }
}
