﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Services.Captcha
{
    public class Captcha
    {
        public string Text { get; set; }
        public Bitmap Image { get; set; }
        
        public Captcha(Bitmap image, string text)
        {
            Text = text;
            Image = image;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Text);
        }

        public byte[] ToArray()
        {
            byte[] arr;
            using(var memoryStram = new MemoryStream())
            {
                Image.Save(memoryStram, ImageFormat.Jpeg);
                arr = memoryStram.ToArray();
            }
            return arr;
        }
    }
}
