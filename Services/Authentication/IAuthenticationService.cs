﻿using Core.Domain.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Authentication
{
    public interface IAuthenticationService
    {
        Task<AppUser> GetAuthenticatedUserAsync(string email);
        Task<UserModel> SignInAsync(AppUser user);
    }
}
