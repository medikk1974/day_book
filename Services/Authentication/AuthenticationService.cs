﻿using Core.Domain.Identity;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace Services.Authentication
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly ITokenService _tokenService;
        public AuthenticationService(UserManager<AppUser> userManager, SignInManager<AppUser> signInManager, ITokenService tokenService)
        {
            _userManager = userManager;
            _tokenService = tokenService;
        }
        public async Task<AppUser> GetAuthenticatedUserAsync(string email)
        {
           return await _userManager.FindByEmailAsync(email);
        }

        public async Task<UserModel> SignInAsync(AppUser user)
        {
            var listRole = await _userManager.GetRolesAsync(user);

            return new UserModel
            {
                Email = user.Email,
                Token = _tokenService.CreateToken(user, listRole),
                DisplayName = user.DisplayName,
                UserRole = listRole
            };
        }
    }
}
