﻿using Core.Domain.Identity;
using System.Collections.Generic;

namespace Services.Authentication
{
    public interface ITokenService
    {
        string CreateToken(AppUser user, IList<string> roles);
    }
}
