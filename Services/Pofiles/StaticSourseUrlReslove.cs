﻿using AutoMapper;
using Core.Domain.Records;
using Microsoft.Extensions.Configuration;
using Services.Records;
using System.Collections.Generic;

namespace Services.Pofiles
{
    class PictureUrlResolve : IValueResolver<Record, RecordModel, List<string>>
    {
        IConfiguration _configuration;

        public PictureUrlResolve(IConfiguration configuration)
        {
            _configuration = configuration;
        }


        public List<string> Resolve(Record source, RecordModel destination, List<string> destMember, ResolutionContext context)
        {
            List<string> str = new List<string>();

            if (source.Pictures != null)
            {
                foreach (var item in source.Pictures)
                {
                    if (!string.IsNullOrEmpty(item.Path))
                    {
                        str.Add(_configuration["ApiUrl"] + item.Path);
                    }
                }
            }
            return str;
        }
    }
}
