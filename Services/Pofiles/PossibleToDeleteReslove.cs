﻿using AutoMapper;
using Core.Domain.Records;
using Microsoft.Extensions.Configuration;
using Services.Records;
using System;

namespace Services.Pofiles
{
    class PossibleToDeleteReslove : IValueResolver<Record, RecordModel, bool>
    {
        IConfiguration _configuration;
        public PossibleToDeleteReslove(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public bool Resolve(Record source, RecordModel destination, bool destMember, ResolutionContext context)
        {
            return DateTime.Now.Subtract(source.DateOfCreation).Days < int.Parse(_configuration["PossibleTimeToDeleteThePost"]);
        }
    }
}
