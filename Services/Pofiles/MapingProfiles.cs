﻿using AutoMapper;
using Core.Domain.Records;
using Services.Cryptology;
using Services.Records;


namespace Services.Pofiles
{
    public class MapingProfiles : Profile
    {
        public MapingProfiles()
        {
            var cryptologyService = new СryptologyService();

            CreateMap<Record, RecordModel>()
               .ForMember(d => d.PossibleToDelete, o => o.MapFrom<PossibleToDeleteReslove>())
               .ForMember(d => d.Content, o => o.MapFrom(e => cryptologyService.Decryption(e.Content)))
               .ForMember(d => d.Title, o => o.MapFrom(e => cryptologyService.Decryption(e.Title)))
               .ForMember(d => d.Pictures, o => o.MapFrom<PictureUrlResolve>());

            CreateMap<RecordForCreateRequest, Record>()
               .ForMember(d => d.Content, o => o.MapFrom(e => cryptologyService.Encryption(e.Content)))
               .ForMember(d => d.Title, o => o.MapFrom(e => cryptologyService.Encryption(e.Title)));
        }
    }
}
