﻿using Core.Domain.Identity;
using Core.Domain.Records;
using Data.DataProviders;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
    public static class SeedTestData
    {

        public static async Task SeedRecordsAsynk(DayBookDbContext dayBookDbContext)
        {
            if (!dayBookDbContext.Records.Any())
            {
                var arrPost = new List<Record>()
                {
                    new Record()
                    {
                       Content = "Post 1",
                       Title = "Title 1",
                       DateOfCreation = DateTime.Parse("01.01.2020"),
                       EmailOwner = "user@gmail.com",
                       Pictures = new List<Picture>()
                       {
                           new Picture()
                           {
                               Path = "iconfinder_user-alt_285645.png"
                           },
                           new Picture()
                           {
                               Path = "iconfinder_user-alt_285645.png"
                           },
                       }
                    },
                };

                dayBookDbContext.Records.AddRange(arrPost);
                await dayBookDbContext.SaveChangesAsync();
            }
        }

        public static async Task SeedUsersAsync(RoleManager<IdentityRole> roleManager, UserManager<AppUser> userManager)
        {
            if (!userManager.Users.Any())
            {
                var admin = new AppUser()
                {
                    DisplayName = "Admin",
                    Email = "admin@gmail.com",
                    UserName = "admin@gmail.com",
                    EmailConfirmed = true,
                };

                var user = new AppUser()
                {
                    DisplayName = "User",
                    Email = "user@gmail.com",
                    UserName = "user@gmail.com",
                    EmailConfirmed = true,
                    //DeltedDate = DateTime.Now.AddDays(-50)
                };

                await userManager.CreateAsync(user, "Pa$$w0rd");
                await userManager.CreateAsync(admin, "Pa$$w0rd");


                await userManager.AddToRoleAsync(admin, "admin");
                await userManager.AddToRoleAsync(user, "user");
            }
        }

        public static async Task SeedRoleAsynk(RoleManager<IdentityRole> roleManager)
        {
            if (!roleManager.Roles.Any())
            {
                var adminRole = new IdentityRole("admin");
                var userrole = new IdentityRole("user");

                await roleManager.CreateAsync(adminRole);
                await roleManager.CreateAsync(userrole);
            }
        }
    }
}
