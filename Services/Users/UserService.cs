﻿using Core.Domain.Identity;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace Services.Users
{
    public class UserService : IUserService
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;

        public UserService(UserManager<AppUser> userManager, SignInManager<AppUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;

        }
        public async Task<AppUser> UserExistsAsync(string email)
        {
            return await _userManager.FindByEmailAsync(email);
        }

        public async Task<IdentityResult> CreateAccount(AppUser user)
        {
            return await _userManager.CreateAsync(user);
        }

        public async Task<AppUser> RegisterUserAsync(AppUser user, RegisterUserDataRequest register) 
        {
            user.Email = register.Email;
            user.EmailConfirmed = true;
            user.DisplayName = register.DisplayName;

            var passwordResetToken = await _userManager.GeneratePasswordResetTokenAsync(user);
            await _userManager.ResetPasswordAsync(user, passwordResetToken, register.Password);
            await _userManager.UpdateAsync(user);

            await _userManager.AddToRoleAsync(user, "user");
            var listRole = await _userManager.GetRolesAsync(user);

            return user;
        }

        public async Task<SignInResult> CheckPasswordSignInAsync(AppUser user, string password, bool v)
        {
            return await _signInManager.CheckPasswordSignInAsync(user, password, true);
        }
    }
}
