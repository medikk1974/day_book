﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Users
{
    public class InvitationUserRequest
    {
        public string AccountId { get; set; }
    }
}
