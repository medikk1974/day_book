﻿using Core.Domain.Identity;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace Services.Users
{
    public interface IUserService
    {
        public Task<AppUser> UserExistsAsync(string email);
        public Task<AppUser> RegisterUserAsync(AppUser user, RegisterUserDataRequest register);
        public Task<IdentityResult> CreateAccount(AppUser user);
        public Task<SignInResult> CheckPasswordSignInAsync(AppUser user, string password, bool v);
    }
}
