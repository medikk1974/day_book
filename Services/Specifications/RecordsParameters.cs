﻿using Services.Cryptology;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Specifications
{
    public class RecordsParameters
    {
        private readonly IСryptologyService _сryptologyService;
        public RecordsParameters()
        {
            _сryptologyService = new СryptologyService();
        }

        private const int MaxPgeSize = 25;
        public int PageIndex { get; set; } = 1;

        private int _pageSize = 5;
        public int PageSize
        {
            get => _pageSize;
            set => _pageSize = (value > MaxPgeSize) ? MaxPgeSize : value;
        }
        public string DataFrom { get; set; }
        public string DataTo { get; set; }

        private DateTime _nullDateTime;
        public DateTime DateTimeFrom
        {
            get
            {
                if (!string.IsNullOrEmpty(DataFrom))
                {
                    return DateTime.Parse(DataFrom);
                };
                return _nullDateTime;
            }
        }
        public DateTime DateTimeTo
        {
            get
            {
                if (!string.IsNullOrEmpty(DataTo))
                {
                    return DateTime.Parse(DataTo);
                }
                return _nullDateTime;
            }
        }
        public string Email { get; set; }

        private string _search;
        public string Search
        {
            get
            {
                return _search;
            }
            set
            {
                _search = _сryptologyService.Encryption(value.ToLower());
            }
        }
    }
}
