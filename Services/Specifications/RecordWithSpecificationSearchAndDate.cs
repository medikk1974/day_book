﻿using Core.Domain.Records;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Specifications
{
    public class RecordWithSpecificationSearchAndDate : BaseSpecification<Record>
    {
        public RecordWithSpecificationSearchAndDate(RecordsParameters postParams) : base
        (x =>
            (x.EmailOwner == postParams.Email) &&
            (string.IsNullOrEmpty(postParams.Search) || x.Content.ToLower().Contains(postParams.Search)) &&
            (postParams.DataFrom == null || (x.DateOfCreation >= postParams.DateTimeFrom && x.DateOfCreation <= postParams.DateTimeTo))
        )

        {
            AddInclude(e => e.Pictures);
            ApplayPaging(postParams.PageSize * (postParams.PageIndex - 1), postParams.PageSize);
            AddOrderByDesc(e => e.DateOfCreation);
        }

        public RecordWithSpecificationSearchAndDate(int id, string email) : base
            (x => (x.Id == id) && (x.EmailOwner == email))
        {
            AddInclude(x => x.Pictures);
        }
    }
}
