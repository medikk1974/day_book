﻿using Core.Domain.Records;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Specifications
{
    public class RecordWithFiltersForCountSpecification : BaseSpecification<Record>
    {
        public RecordWithFiltersForCountSpecification(RecordsParameters postParams) : base
          (e =>
              (e.EmailOwner == postParams.Email) &&
              (string.IsNullOrEmpty(postParams.Search) || e.Content.ToLower().Contains(postParams.Search)) &&
              (postParams.DataFrom == null || (e.DateOfCreation >= postParams.DateTimeFrom && e.DateOfCreation <= postParams.DateTimeTo))
          )
        {

        }
    }
}
