﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Threading.Tasks;

namespace Services.Images
{
    public class ImageService : IImageService
    {
        public const int MAX_IMAGE_SIZE = 5242880; //5mb
        public ImageService()
        {

        }

        public async Task<string> SavePicture(Stream stream, string filename, string wwwrootpath)
        {
            var path = Guid.NewGuid().ToString() + Path.GetExtension(filename);
            var absolutePath = wwwrootpath + "\\" + path;

            if (stream.Length > MAX_IMAGE_SIZE)
            {
                ReduceImageSize(0.7, stream, absolutePath);
            }
            else
            {
                using (var fileStream = new FileStream(absolutePath, FileMode.Create))
                {
                    await stream.CopyToAsync(fileStream);
                }
            }

            return path;
        }

        public void DeletePicture(string path, string wwwrootpath)
        {
            var filePath = wwwrootpath + "\\" + path;
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
        }


        private void ReduceImageSize(double scaleFactor, Stream sourcePath, string targetPath)
        {
            using (var image = System.Drawing.Image.FromStream(sourcePath))
            {
                var newWidth = (int)(image.Width * scaleFactor);
                var newHeight = (int)(image.Height * scaleFactor);
                var thumbnailImg = new Bitmap(newWidth, newHeight);
                var thumbGraph = Graphics.FromImage(thumbnailImg);
                thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                var imageRectangle = new System.Drawing.Rectangle(0, 0, newWidth, newHeight);
                thumbGraph.DrawImage(image, imageRectangle);
                thumbnailImg.Save(targetPath, image.RawFormat);
            }
        }

    }
}
