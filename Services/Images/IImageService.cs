﻿using System.IO;
using System.Threading.Tasks;

namespace Services.Images
{
    public interface IImageService
    {
        Task<string> SavePicture(Stream stream, string filename, string wwwrootpath);
        void DeletePicture(string path, string wwwrootpath);
    }
}
