﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Services.Records
{
    public class RecordForCreateRequest
    {
        [Required]
        public string Title { get; set; }

        [Required]
        [StringLength(maximumLength: 500 , ErrorMessage = "Content should not be more than 500 characters")]
        public string Content { get; set; }
        public IFormFile FileToUploade { get; set; }
    }
}
