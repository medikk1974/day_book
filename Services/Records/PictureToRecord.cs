﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Services.Records
{
    public class PictureToRecord
    {
        public IFormFile File { get; set; }

        [Required]
        public int RecordId { get; set; }
    }
}
