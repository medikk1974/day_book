﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Cryptology
{
    public class СryptologyService : IСryptologyService
    {
        public string Encryption(string corectSting)
        {
            var charArr = corectSting.ToCharArray();
            for (int i = 0; i < charArr.Length; i++)
            {
                ++charArr[i];
            }
            return new string(charArr);
        }

        public string Decryption(string encryptionString)
        {
            var charArr = encryptionString.ToCharArray();
            for (int i = 0; i < charArr.Length; i++)
            {
                --charArr[i];
            }
            return new string(charArr);
        }
    }
}
