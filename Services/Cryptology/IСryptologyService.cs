﻿namespace Services.Cryptology
{
    public interface IСryptologyService
    {
        string Encryption(string corectSting);
        string Decryption(string encryptionString);
    }
}
