import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {ILogin, IRegistrationUser, IUser} from '../shared/Models/User';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AccuntService {

  private currentUserSource = new BehaviorSubject<IUser>(null);
  curentUser$ = this.currentUserSource.asObservable();

  private baseUrl = 'http://localhost:5000/api/account';

  constructor(private http: HttpClient) {
  }

  checkingСaptcha(captcha:string){
    return  this.http.post<boolean>("http://localhost:5000/api/captcha?captcha="+captcha,
      undefined, { withCredentials: true} )
  }

  login(value: ILogin){
    return this.http.post<IUser>(this.baseUrl+ '/login', value)
      .pipe(map((user: IUser)=>{
        localStorage.setItem('token',user.token);
        this.currentUserSource.next(user);
        return user;
      }))
  }

  logout() {
    localStorage.removeItem("token");
    this.currentUserSource.next(null);
  }

  loadCurrentUser(token: string){
    console.log(token);
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', 'Bearer '+token)

    return this.http.get(this.baseUrl, {headers})
      .pipe(map((user: IUser)=>{
        if(user){
          localStorage.setItem('token',user.token)
          this.currentUserSource.next(user);
          return user;
        }
      }));
  }

  checkEmailExists(email:string){
    return this.http.get<boolean>(this.baseUrl+"/emailexists"+"?email="+email);
  }

  checkInvitation(AccountId: string) {
    return this.http.get<boolean>(this.baseUrl+"/checkInvitation"+"?AccountId="+AccountId);
  }

  registerAccount(registrationUser: IRegistrationUser){
    return this.http.post<IUser>(this.baseUrl+"/register", registrationUser)
      .pipe(map((user: IUser)=>{
        localStorage.setItem('token',user.token);
        this.currentUserSource.next(user);
        return user;
      }))
  }
}
