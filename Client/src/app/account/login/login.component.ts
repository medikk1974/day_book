import { Component, OnInit } from '@angular/core';
import {AccuntService} from '../accunt.service';
import {ILogin, IUser} from '../../shared/Models/User';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor(private accountService: AccuntService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      "email": new FormControl(null,[Validators.required, Validators.email]),
      "password": new FormControl(null,[Validators.required] ),
    });
  }


  onSubmit(){
    const loginData: ILogin = {
      email: this.loginForm.value["email"],
      password: this.loginForm.value["password"],
    }

    this.accountService.login(loginData)
      .subscribe(response =>{
        if(response.userRole.includes("user")){
          this.router.navigate(['/day-book']);
        }else if(response.userRole.includes("admin")){
          this.router.navigate(['/admin'])
        }
      });
  }
}
