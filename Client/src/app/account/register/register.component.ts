import {Component, ElementRef, OnInit, TemplateRef, ViewChild, ViewContainerRef} from '@angular/core';
import {AsyncValidatorFn, FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {IRegistrationUser} from '../../shared/Models/User';
import {ActivatedRoute, Router} from '@angular/router';
import {AccuntService} from '../accunt.service';
import {of, timer} from "rxjs";
import {map, switchMap} from "rxjs/operators";


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {


  registerForm: FormGroup;

  private AccountId: string;
  captchaIscorrect: boolean;

  imgCapture: string ='http://localhost:5000/api/Captcha';

  constructor( private activateRoute: ActivatedRoute,
               private accuntService: AccuntService,
               private router: Router) {
  }

  ngOnInit(): void {

    this.checkInvitation();

    this.registerForm = new FormGroup({
      "email": new FormControl(null, [Validators.email, Validators.required],[this.validateEmailNotTaken()]),
      "password": new FormControl(null, [Validators.required, Validators.minLength(6)]),
      "username":new FormControl(null,[Validators.required]),
      "captcha":new FormControl(null, [Validators.required],)
    });
  }

  submit() {
    const registrationData: IRegistrationUser = {
      AccountId: this.AccountId,
      DisplayName: this.registerForm.value["username"],
      Email: this.registerForm.value["email"],
      Password: this.registerForm.value["password"],
    }

    this.accuntService.registerAccount(registrationData).subscribe(user=>{
      if(user.userRole.includes("admin")){
        this.router.navigate(['/admin']);
      }else if(user.userRole.includes("user")){
        this.router.navigate(['/day-book']);
      }
    });
  }

  myclick() {
    console.log(this.registerForm.value["captcha"]);
    this.accuntService.checkingСaptcha(this.registerForm.value["captcha"])
      .subscribe(response=>{
        if(response){
          this.captchaIscorrect = true;
        }else{
          this.imgCapture +="?" + new Date().toDateString();
        }
      });
  }

  validateEmailNotTaken(): AsyncValidatorFn {
    return control => {
      return timer(500).pipe(switchMap(()=>{
        if(control.value==null){
          return of(null);
        }
        return this.accuntService.checkEmailExists(control.value).pipe(map(res=>{
          return res ? {emailExists: true} : null
        }))
      }))
    }
  }

  checkInvitation(){
    this.activateRoute.queryParams.subscribe(
      (queryParam: any) => {
        this.AccountId = queryParam['AccountId'];
      }
    );

    this.accuntService.checkInvitation(this.AccountId).subscribe(response=>{
      if(!response){
        this.router.navigate(['/errorPage']);
      }
    });
  }
}
