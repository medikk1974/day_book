import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {InvitationForRegistrationComponent} from './invitation-for-registration/invitation-for-registration.component';
import {AdminGuard} from "../core/guard/admin.guard";

const routes: Routes = [
  {path: "", component: InvitationForRegistrationComponent, canActivate: [AdminGuard]} ];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports:[RouterModule]
})

export class AdminRoutingModule { }
