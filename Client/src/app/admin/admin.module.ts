import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvitationForRegistrationComponent } from './invitation-for-registration/invitation-for-registration.component';
import {AdminRoutingModule} from './admin-routing.module';
import {SharedModule} from "../shared/shared.module";

@NgModule({
  declarations: [InvitationForRegistrationComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    SharedModule
  ]
})
export class AdminModule { }
