import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  private baseUrl = 'http://localhost:5000/api/Account/create';

  constructor(private http: HttpClient) {
  }

  sendInvitation(email:string ){
    console.log("++")
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', 'Bearer '+localStorage.getItem("token"))
    return this.http.post<any>(this.baseUrl,{ Email: email },{headers});
  }

}
