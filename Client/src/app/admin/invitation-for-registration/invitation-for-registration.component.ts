import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AccuntService} from "../../account/accunt.service";
import {AdminService} from "../admin.service";

@Component({
  selector: 'app-invitation-for-registration',
  templateUrl: './invitation-for-registration.component.html',
  styleUrls: ['./invitation-for-registration.component.scss']
})
export class InvitationForRegistrationComponent implements OnInit {

  invitation: FormGroup;
  constructor(private adminService: AdminService) { }

  ngOnInit(): void {
    this.invitation = new FormGroup({
      "email": new FormControl(null, [Validators.email, Validators.required])
    });
  }

  submit(){
    this.adminService.sendInvitation(this.invitation.value["email"]).subscribe();
  }
}
