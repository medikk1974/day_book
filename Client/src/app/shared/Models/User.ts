export interface IUser{
  email:string,
  displayName:string,
  token:string,
  userRole:string[],
}

export interface IPost{
  id:string;
  title:string;
  content:string;
  pictures:string[];
  dateOfCreation:Date;
  possibleToDelete:string;
}

export interface ILogin{
  email:string;
  password:string;
}

export class SerchParam {
  pageNumber = 1;
  pageSize = 5;
  search:string;

  dataFrom:string;
  dataTo:string;

  set DataRange(date: Date[]){
    // Звязано з календарем в ньому немаэ можливосты тримати одну дату
    if(date){

      let monthfirst: number = +date[0].getMonth()+1;
      let monthsecond: number = +date[1].getMonth()+1;

      this.dataFrom = date[0].getDate()+"."+ monthfirst +"."+ date[0].getFullYear();
      this.dataTo = date[1].getDate()+"."+ monthsecond +"."+ date[1].getFullYear();

      console.log(this.dataFrom +"  "+this.dataTo)
    }
  }
}

export interface IPagination {
  pgeIndex: number;
  pageSize: number;
  count: number;
  data: IPost[];
}

export  interface IRegistrationUser{
  AccountId : string;
  DisplayName: string;
  Email:string;
  Password:string;
}
