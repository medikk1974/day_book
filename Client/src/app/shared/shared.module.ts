import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BsDatepickerModule} from 'ngx-bootstrap/datepicker';
import {PaginationModule} from 'ngx-bootstrap/pagination';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import { ErrorPageComponent } from '../core/error-page/error-page.component';

@NgModule({
  declarations: [ErrorPageComponent],
  imports: [
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    BsDatepickerModule.forRoot(),
    PaginationModule.forRoot(),
    BsDropdownModule.forRoot(),
  ],
  exports: [FormsModule, ReactiveFormsModule, BsDatepickerModule, PaginationModule,BsDropdownModule, ErrorPageComponent]
})
export class SharedModule { }
