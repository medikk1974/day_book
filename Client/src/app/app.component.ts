import { Component } from '@angular/core';

import {AccuntService} from './account/accunt.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {

  constructor(private accountService: AccuntService) {
    if(localStorage.getItem("token")){
      this.accountService.loadCurrentUser(localStorage.getItem("token")).subscribe(response=>
        console.log(response)
      );
    }
  }
}
