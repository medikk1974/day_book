import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {IPost, SerchParam} from '../../shared/Models/User';
import {BookDaysService} from '../book-days.service';
import {ActivatedRoute, Router} from "@angular/router";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-post-page',
  templateUrl: './post-page.component.html',
  styleUrls: ['./post-page.component.scss']
})
export class PostPageComponent implements OnInit {

  curentPost: IPost;
  addPictureForm: FormGroup;

  private id: string;
  private fileToUploade: File = null;

  get selectFileName():string{
    return this.fileToUploade==null? "Оберіть фото": this.fileToUploade.name;
  }

  constructor(private activateRoute: ActivatedRoute,
              private daysBookServices: BookDaysService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.id = this.activateRoute.snapshot.params['id'];
    this.loadCurentPost();

    this.addPictureForm = new FormGroup({
      "file":new FormControl(null, Validators.required),
    })
  }

  loadCurentPost(){
    this.daysBookServices.getPostById(this.id).subscribe(res=>{
      this.curentPost = res;
    },error => {
      this.router.navigate(["/day-book"])
    })
  }

  hendelFileInput(file: FileList){
    this.fileToUploade = file.item(0);
  }

  addPicture(){
    const formData = new FormData();
    formData.append("file", this.fileToUploade, this.fileToUploade.name)
    formData.append("recordId", this.id)

    this.daysBookServices.addPicture(formData).subscribe(resp=>{
      this.addPictureForm.setValue({ file:null})
      this.fileToUploade = null;
      this.curentPost = resp
    }, error => {

    })
  }
}
