import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {IPagination, IPost, IUser, SerchParam} from '../shared/Models/User';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BookDaysService {

  private baseUrl = 'http://localhost:5000/api';
  constructor(private http: HttpClient) {

  }

  getPosts(serchParam:SerchParam):Observable<IPagination> {
    let params = new HttpParams();

    let headers = new HttpHeaders();
    headers = headers.set('Authorization', 'Bearer '+localStorage.getItem("token"))

    if (serchParam.dataFrom && serchParam.dataTo) {
      params = params.append('DataFrom', serchParam.dataFrom);
      params = params.append('DataTo', serchParam.dataTo);
    }

    if (serchParam.search) {
      params = params.append('search', serchParam.search.toString());
    }

    params = params.append("PageIndex", serchParam.pageNumber.toString());
    params = params.append('PageSize', serchParam.pageSize.toString());

    return  this.http.get<IPagination>(this.baseUrl + "/record",
      { params, headers});
  }

  createPost(formData: FormData){
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', 'Bearer '+localStorage.getItem("token"))
    return  this.http.post(this.baseUrl+"/record", formData, {headers})
  }

  addPicture(formData: FormData){
    console.log(formData)
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', 'Bearer '+localStorage.getItem("token"))
    return this.http.post<IPost>(this.baseUrl+"/record/picture", formData, {headers})
  }

  deletePost(id: string) : Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', 'Bearer '+localStorage.getItem("token"))
    return this.http.delete<any>(this.baseUrl+"/record/"+id, {headers})
  }

  getPostById(id: string) {
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', 'Bearer '+localStorage.getItem("token"))
    return this.http.get<IPost>(this.baseUrl+"/record/"+id, {headers})
  }
}
