import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostPageComponent } from './post-page/post-page.component';
import {BookDaysRoutingModule} from './book-days-routing.module';
import {CreatePostComponent } from './create-post/create-post.component';
import {SharedModule} from '../shared/shared.module';
import {PostsComponent} from './posts/posts.component';


@NgModule({
  declarations: [PostPageComponent, PostsComponent, CreatePostComponent],
  imports: [
    CommonModule,
    BookDaysRoutingModule,
    SharedModule,
  ]
})
export class BookDaysModule { }
