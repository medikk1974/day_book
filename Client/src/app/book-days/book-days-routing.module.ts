import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {PostPageComponent} from './post-page/post-page.component';
import {PostsComponent} from './posts/posts.component';
import {CreatePostComponent} from './create-post/create-post.component';
import {UserGuard} from "../core/guard/user.guard";
import {AdminGuard} from "../core/guard/admin.guard";


const routes: Routes = [
  {path: "post/:id", component: PostPageComponent, canActivate: [UserGuard]},
  {path: "", component: PostsComponent, canActivate: [UserGuard]},
  {path: "create-post", component: CreatePostComponent, canActivate: [UserGuard]}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports:[RouterModule]
})
export class BookDaysRoutingModule { }
