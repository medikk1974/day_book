import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {BookDaysService} from "../book-days.service";

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.scss']
})
export class CreatePostComponent implements OnInit {

  createPost: FormGroup;
  fileToUploade: File = null;

  get selectFileName():string{
    return this.fileToUploade==null? "Оберіть фал": this.fileToUploade.name;
  }

  constructor(private dookDaysService: BookDaysService ) { }

  ngOnInit(): void {
    this.createPost = new FormGroup({
      "file":new FormControl(null),
      "title":new FormControl(null, [Validators.required,Validators.maxLength(200)]),
      "text":new FormControl(null,[Validators.required,Validators.maxLength(500)])
    })
  }

  hendelFileInput(file: FileList){
    this.fileToUploade = file.item(0);
  }

  submit() {
    const formData = new FormData();
    if(this.fileToUploade!=null){
      formData.append('FileToUploade', this.fileToUploade, this.fileToUploade.name)
    }
    formData.append('Title', this.createPost.value['title'])
    formData.append('Content', this.createPost.value['text'])
    this.dookDaysService.createPost(formData).subscribe(res=>{}, exr=>{
      console.log(exr);
    });
  }
}
