import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {IPost, SerchParam} from '../../shared/Models/User';
import {BookDaysService} from '../book-days.service';
import {THIS_EXPR} from '@angular/compiler/src/output/output_ast';
import {strict} from "assert";

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {

  @ViewChild("search",{static:true}) serchTerm: ElementRef;

  posts: IPost[] = [];
  serchParam: SerchParam = new SerchParam();
  bsInlineRangeValue: Date[];
  totalcount = 0;

  constructor(private bookDaysService: BookDaysService) { }

  ngOnInit(){
    this.getPosts();
  }

  pageChanged(page: any) {

    if(page != this.serchParam.pageNumber){
      this.serchParam.pageNumber = page.page;
      this.getPosts();
    }
  }

  getPosts(){
    this.bookDaysService.getPosts(this.serchParam).subscribe(response=>{
      this.totalcount = response.count;
      this.posts = response.data;
      this.serchParam.pageSize = response.pageSize;
    });
  }

  onSearch() {
    this.serchParam.DataRange = this.bsInlineRangeValue
    this.serchParam.search = this.serchTerm.nativeElement.value;
    this.getPosts();
  }

  onReset(){
    this.serchTerm.nativeElement.value = "";
    this.serchParam = new SerchParam();
    this.bsInlineRangeValue = undefined;
    this.getPosts();
  }


  deletePost(post: IPost) {
    this.bookDaysService.deletePost(post.id).subscribe(resp=>{
      const index = this.posts.indexOf(post);
      this.posts.splice(index,1);
      console.log(resp)
    })
  }
}
