import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {ErrorPageComponent} from "./core/error-page/error-page.component";
import {AdminGuard} from "./core/guard/admin.guard";
import {UserGuard} from "./core/guard/user.guard";

const routes: Routes = [
  {path:"account", loadChildren:()=>import('./account/account.module').then(mod=>mod.AccountModule)},
  {path:"day-book", loadChildren:()=> import('./book-days/book-days.module').then(mod=>mod.BookDaysModule)},
  {path:"admin", loadChildren:()=>import('./admin/admin.module').then(mod=>mod.AdminModule)},
  {path:'errorPage', component: ErrorPageComponent},
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ]
})
export class AppRoutingModule { }
