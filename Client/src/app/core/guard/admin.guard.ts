import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs";
import {AccuntService} from "../../account/accunt.service";
import {IUser} from "../../shared/Models/User";
import {Injectable} from "@angular/core";
import {map} from "rxjs/operators";

@Injectable()
export class AdminGuard implements CanActivate{

  curentUser$ : Observable<IUser>

  constructor(private accuntService: AccuntService) {
    this.curentUser$ = accuntService.curentUser$;
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) : Observable<boolean> | boolean{
    return this.curentUser$.pipe(map((user: IUser)=>{
      if(user.userRole.includes("admin")){
        return true;
      }
      return false;
    }))
    return false;
  }
}
