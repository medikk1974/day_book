import { Component, OnInit } from '@angular/core';
import {AccuntService} from '../../account/accunt.service';
import {Observable} from 'rxjs';
import {IUser} from '../../shared/Models/User';
import {Router} from "@angular/router";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  curentUser$: Observable<IUser>;

  constructor(private accountService: AccuntService,
              private router: Router) {
        this.curentUser$ = this.accountService.curentUser$
  }

  ngOnInit(): void {

  }

  logOut() {
    this.accountService.logout();
    this.router.navigate(['/account/login']);
  }
}
