# Шоденик

# Task 
веб-застосунок "мій особистий щоденник" з застосуванням
наступних технологій:

1) asp.net core
2) EF Core code-first approach
3) RESTful service
4) MS SQL
5) Angular or RAZOR 

Реалізувати наступну логіку
1) реєстрація користувача можлива лише по інвайту, адмін відправляє
інвайт на пошту користувачеві. Реєстрація запрошує нікнейм (публічне
поле), емайл(тільки для входу) пароль та капча (створити самостійно).
2) сторінка логіну просить пароль, логін
3) після входу юзер має можливість:
   * додати новий запис (не більше 500 символів)
   * шукати по контенту
   * шукати по даті (date range)
   * видаляти записи
   * додавати картинку
4) записи старші 2х днів видалити не можна
5) юзер може повністю видалити акаунт, причому фізично дані видаляться
лише через 2 доби. За цей період юзер можна відновити акаунт
6) картинки мають зберігатися оптимізованими (якшо залили 10 мбайт то
зберігати стиснуту версію)
7) створити RESTful API якийсь дозволить створити застосунок на мобайл
пристрої. Методи - getRecords, getRecordById тощо

Технічні деталі
1) асп.нет сесія зберігається в БД або GWT
2) весь контент щоденника має бути зашифровано в БД на випадок взлому
3) на сторінці має показуватися 5 записів, використати пейджінг
(посторінковий вивід)
4) id для public API мають бути не числові

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
