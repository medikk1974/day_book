﻿using Data;
using Microsoft.Extensions.DependencyInjection;
using Services.Authentication;
using Services.Captcha;
using Services.Cryptology;
using Services.Images;
using Services.Pofiles;
using Services.Users;

namespace Api.Extensions
{
    public static class ApplicationServiceExtensions
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            services.AddScoped(typeof(IRepository<>), typeof(EntityRepository<>));


            services.AddScoped(typeof(IRepository<>), typeof(EntityRepository<>));
            services.AddAutoMapper(typeof(MapingProfiles));
            services.AddSingleton<IImageService, ImageService>();
            services.AddSingleton<IСryptologyService, СryptologyService>();
            services.AddScoped<IRandomCaptchaGenerator, RandomCaptchaGenerator>();
            services.AddScoped<IAuthenticationService, AuthenticationService>();
            services.AddSingleton<ITokenService, TokenService>();
            services.AddScoped<IUserService, UserService>();

            return services;
        }
    }
}
