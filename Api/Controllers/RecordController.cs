﻿using AutoMapper;
using Core;
using Core.Domain.Records;
using Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Services.Records;
using Services.Images;
using Microsoft.AspNetCore.Hosting;

namespace Api.Controllers
{
    [Authorize(Roles = "user")]
    public class RecordController : BaseApiController
    {
        private readonly IRepository<Record> _recordsRepository;
        private readonly IRepository<Picture> _picturesRepository;
        public readonly IMapper _mapper;
        public readonly IImageService _imageService;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public RecordController(IRepository<Record> repositoryRecord, IRepository<Picture> repositoryPicture,
            IImageService imageService, IMapper maper, IWebHostEnvironment webHostEnvironment)
        {
            _recordsRepository = repositoryRecord;
            _picturesRepository = repositoryPicture;
            _imageService = imageService;
            _mapper = maper;
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpGet]
        public async Task<ActionResult<Pagination<RecordModel>>> GetRecords([FromQuery] RecordsParameters recordSearchParam)
        {
            var email = HttpContext.User?.Claims?
                .FirstOrDefault(e => e.Type == ClaimTypes.Email).Value;

            recordSearchParam.Email = email;
            
            var searchSpec = new RecordWithSpecificationSearchAndDate(recordSearchParam);
            var specForCount = new RecordWithFiltersForCountSpecification(recordSearchParam);
            
            var count = await _recordsRepository.CountAsync(specForCount);
            var items = await _recordsRepository.ListAsync(searchSpec);

            var posts = _mapper.Map<IReadOnlyList<Record>, IReadOnlyList<RecordModel>>(items);
            return Ok(new Pagination<RecordModel>(recordSearchParam.PageIndex, recordSearchParam.PageSize, count, posts));
        }

        [HttpPost]
        public async Task<ActionResult<RecordModel>> CreateRecord([FromForm] RecordForCreateRequest recordForCreateRequest)
        {
            var email = HttpContext.User?.Claims?
                .FirstOrDefault(e => e.Type == ClaimTypes.Email).Value;

            var record = _mapper.Map<RecordForCreateRequest, Record>(recordForCreateRequest);
            record.EmailOwner = email;
            record.DateOfCreation = DateTime.Now;

            if (recordForCreateRequest.FileToUploade != null)
            {
                string pathToPicture = await _imageService.SavePicture(recordForCreateRequest.FileToUploade.OpenReadStream(),
                    recordForCreateRequest.FileToUploade.FileName, _webHostEnvironment.WebRootPath);
               
                var picture = new Picture
                {
                    Path = pathToPicture,
                };

                record.Pictures.Add(picture);
            }

            _recordsRepository.Add(record);
            if (await _recordsRepository.SaveChangesAsync() <= 0)
            {
                return null;
            }

            return Ok(_mapper.Map<Record, RecordModel>(record));
        }

        [HttpPost("picture")]
        public async Task<ActionResult<RecordModel>> AddPictureToPost([FromForm] PictureToRecord uploadedFile)
        {
            if (uploadedFile != null)
            {
                var email = HttpContext.User.Claims
                    .FirstOrDefault(e => e.Type == ClaimTypes.Email).Value;

                var sepc = new RecordWithSpecificationSearchAndDate(uploadedFile.RecordId, email);
                var record = await _recordsRepository.GetEntityWithSpec(sepc);

                if (record != null)
                {
                    var pathToFile = await _imageService.SavePicture(uploadedFile.File.OpenReadStream(),
                    uploadedFile.File.FileName, _webHostEnvironment.WebRootPath);

                    _picturesRepository.Add(new Picture()
                    {
                        PostId = record.Id,
                        Path = pathToFile,
                    });

                    _recordsRepository.Update(record);
                    await _recordsRepository.SaveChangesAsync();
                }
                else
                {
                    return NotFound();
                }

                return Ok(_mapper.Map<Record, PictureToRecord>(record));
            }
            return BadRequest();
        }


        [HttpGet("{id}")]
        public async Task<ActionResult<RecordModel>> GetRecordById(string id)
        {
            var email = HttpContext.User?.Claims?
                .FirstOrDefault(e => e.Type == ClaimTypes.Email).Value;

            var sepc = new RecordWithSpecificationSearchAndDate(int.Parse(id), email);
            var record = await _recordsRepository.GetEntityWithSpec(sepc);

            if (record == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<Record, RecordModel>(record));
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteRecord(int id)
        {
            var record = await _recordsRepository.GetByIdAsynk(id);
            if (record == null)
            {
                return NotFound();
            }

            var mapRecord = _mapper.Map<Record, RecordModel>(record);

            if (mapRecord.PossibleToDelete)
            {
                _recordsRepository.Delete(record);
                if (await _recordsRepository.SaveChangesAsync() <= 0)
                {
                    return BadRequest();
                }
            }
            return Ok();
        }
    }
}
