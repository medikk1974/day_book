﻿using Core.Domain.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NETCore.MailKit.Core;
using Services.Authentication;
using Services.Users;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Api.Controllers
{
    public class AccountController : BaseApiController
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly IUserService _userService;
        private readonly IEmailService _emailService;
        private readonly IConfiguration _configuration;
        private readonly UserManager<AppUser> _userManager;
        public AccountController(IAuthenticationService authenticationService, IEmailService emailService, 
            IConfiguration configuration, IUserService userService, UserManager<AppUser> userManager)
        {
            _authenticationService = authenticationService;
            _emailService = emailService;
            _userService = userService;
            _configuration = configuration;
            _userManager = userManager;
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult<UserModel>> GetCarrentUser()
        {
            var email = HttpContext.User?.Claims?
                .FirstOrDefault(e => e.Type == ClaimTypes.Email).Value;

            var user = await _authenticationService.GetAuthenticatedUserAsync(email);

            return await _authenticationService.SignInAsync(user);
        }

        [HttpGet("emailexists")]
        public async Task<ActionResult<bool>> CheckEmailExistsAsync([FromQuery] string email)
        {
            return await _userService.UserExistsAsync(email) != null;
        }

        [HttpPost("register")]
        public async Task<ActionResult<UserModel>> Register(RegisterUserDataRequest register)
        {
            var user = await _userManager.FindByIdAsync(register.AccountId);

            if (user == null || user.EmailConfirmed)
            {
                return BadRequest();
            }

            await _userService.RegisterUserAsync(user, register);

            return await _authenticationService.SignInAsync(user);
        }

        [HttpPost("login")]
        public async Task<ActionResult<UserModel>> Login(LoginDataRequest logindata)
        {
            var user = await _userService.UserExistsAsync(logindata.Email);
            if (user == null || !user.EmailConfirmed)
            {
                return Unauthorized();
            }

            var result = await _userService.CheckPasswordSignInAsync(user, logindata.Password, true);

            if (!result.Succeeded)
            {
                return Unauthorized();
            }

            return await _authenticationService.SignInAsync(user);
        }

        [HttpGet("checkInvitation")]
        public async Task<ActionResult<bool>> CheckConfirmEmail([FromQuery] InvitationUserRequest invite)
        {
            var user = await _userManager.FindByIdAsync(invite.AccountId);

            if (user == null || user.EmailConfirmed)
            {
                return false;
            }

            return true;
        }

        [Authorize(Roles = "admin")]
        [HttpPost("create")]
        public async Task<ActionResult<string>> CreateAccount(Invite email)
        {
            var user = new AppUser
            {
                UserName = Guid.NewGuid().ToString(),
            };
            var result = await _userService.CreateAccount(user);

            if (!result.Succeeded)
            {
                return BadRequest();
            }

            var emailContent = $" <a  href=\"{_configuration.GetValue<string>("FrontEnd")}?AccountId={user.Id}\"> Перейти до реэстрації </a>";

            await _emailService.SendAsync(email.Email, "Дозвіл на реэстрацію", emailContent, true);

            return NoContent();
        }
    }

    public class Invite
    {
        public string Email { set; get; }
    }
}
