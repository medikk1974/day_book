﻿using Microsoft.AspNetCore.Mvc;
using Services.Captcha;
using System;

namespace Api.Controllers
{
    public class CaptchaController : BaseApiController
    {
        public IRandomCaptchaGenerator _captchaGenerator;
        public CaptchaController(IRandomCaptchaGenerator captchaGenerator)
        {
            _captchaGenerator = captchaGenerator;   
        }

        [HttpGet]
        public ActionResult Captcha()
        {
            var captcha = _captchaGenerator.CreatCaptcha();
            
            Response.ContentType = "image/jpeg";
            Response.Cookies.Append("Captcha", captcha.GetHashCode().ToString());
            
            return new FileContentResult(captcha.ToArray(), "image/jpeg");
        }

        [HttpPost]
        public ActionResult<bool> ValidCaptcha(string captcha)
        {
            int captchaId=-1;
            string name = Request.Cookies["Captcha"];

            if (Request.Cookies.ContainsKey("Captcha"))
            {
                captchaId = int.Parse(Request.Cookies["Captcha"]);
            }

            return HashCode.Combine(captcha) == captchaId ? true : false;
        }
    }
}
