using System;
using System.Threading.Tasks;
using Core.Domain.Identity;
using Data.DataProviders;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Services;

namespace Api
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            using(var scope = host.Services.CreateScope())
            {
                var service = scope.ServiceProvider;
                var logerFactory = service.GetRequiredService<ILoggerFactory>();
                try
                {
                    var dayBookContext = service.GetRequiredService<DayBookDbContext>();
                    await dayBookContext.Database.MigrateAsync();

                    var identityContext = service.GetRequiredService<AppIdentityDbContext>();
                    await identityContext.Database.MigrateAsync();

                    var roleManager = service.GetRequiredService<RoleManager<IdentityRole>>();
                    await SeedTestData.SeedRoleAsynk(roleManager);

                    var userManager = service.GetRequiredService<UserManager<AppUser>>();
                    await SeedTestData.SeedUsersAsync(roleManager, userManager);

                    await SeedTestData.SeedRecordsAsynk(service.GetRequiredService<DayBookDbContext>());
                }
                catch (Exception ex)
                {
                    var loger = logerFactory.CreateLogger<Program>();
                    loger.LogError(ex, "Failed to create migration");
                }
            }
            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
