﻿using Core.Domain.Records;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.DataProviders
{
    public class DayBookDbContext : DbContext
    {
        public DbSet<Record> Records { get; set; }
        public DbSet<Picture> Pictures { get; set; }
        public DayBookDbContext(DbContextOptions<DayBookDbContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
    }
}
