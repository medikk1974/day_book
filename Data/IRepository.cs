﻿using Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data
{
    public interface IRepository<Tentity> where Tentity : BaseEntity
    {
        Task<Tentity> GetByIdAsynk(int id);
        Task<IReadOnlyList<Tentity>> ListAllAsync();
        void Add(Tentity entity);
        void Update(Tentity entity);
        void Delete(Tentity entity);
        Task<int> SaveChangesAsync();
        Task<Tentity> GetEntityWithSpec(ISpecification<Tentity> specification);
        Task<IReadOnlyList<Tentity>> ListAsync(ISpecification<Tentity> specification);
        Task<int> CountAsync(ISpecification<Tentity> specification);
    }
}
