﻿using System;
using System.Collections.Generic;

namespace Core
{
    public class Pagination<T> where T : class
    {
        public Pagination(int pgeIndex, int pageSize, int count, IReadOnlyList<T> data)
        {
            PgeIndex = pgeIndex;
            PageSize = pageSize;
            Count = count;
            Data = data ?? throw new ArgumentNullException(nameof(data));
        }

        public int PgeIndex { get; set; }
        public int PageSize { get; set; }
        public int Count { get; set; }
        public IReadOnlyList<T> Data { get; set; }
    }
}
