﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.Records
{
    public class Record : BaseEntity
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public string EmailOwner { get; set; }
        public DateTime DateOfCreation { get; set; }
        public DateTime? DateOfDeletetion { get; set; }
        public List<Picture> Pictures { get; set; } = new List<Picture>();
    }
}
