﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.Records
{
    public class Picture : BaseEntity
    {
        public string Path { get; set; }
        public Record Post { get; set; }
        public int PostId { get; set; }
    }
}
