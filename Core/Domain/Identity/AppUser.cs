﻿using System;
using Microsoft.AspNetCore.Identity;

namespace Core.Domain.Identity
{
    public class AppUser : IdentityUser
    {
        public string DisplayName { get; set; }
        public DateTime? DeltedDate { get; set; }
    }
}
